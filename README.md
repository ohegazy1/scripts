# scripts

## Migrate Accounts From Holdings to Order Accounts
### accounts/src/main/scala/com/scripts/accounts/MigrateAccounts
Takes all orders accounts from `pipg.holdings` and sends requests to `MigrateAccountsService.scala` to copy them over to
`pipg.orderAccounts`

Make sure to fill out `HARD_CODED_VARIABLES` on L28: 
```scala
  private final val HARD_CODED_VARIABLES = ScriptConfig(
  networksDoneFolder = "/your/folder/here", // eg: /Users/bsmith/Documents
  env = DEV, //DEV, QA, or PROD
  tokenSSO = "Simon SSO token string", // get it from Postman, or https://www.{env}.simonmarkets.com/simon/api/config/ui/authtoken
  mongoDbEmail = "yourmongoemail@here.com", // code will take care of encoding this for you
  mongoDbPassword = "yourmongopasswordhere", // this will also get encoded for you
  networks = List.empty[String], // (optional), hardcoded list of networks to migrate 
  batchSize = 1000, //(optional) size of accounts page
  checkMongo = false, //(optional) checks pipg.accounts to see if something's missing from pipg.ordersAccounts
  resetCache = false //(optional) clears out all the networksDone JSON which batches were already migrated on which networks
)
```