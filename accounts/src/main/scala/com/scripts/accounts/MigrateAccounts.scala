package com.scripts.accounts

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.http.scaladsl.model.headers.RawHeader
import com.scripts.accounts.MigrateAccountsHelpers.Environment._
import com.scripts.accounts.MigrateAccountsHelpers.{MigrateNetworkAccountsRequest, ScriptConfig}
import com.simonmarkets.http.FutureHttpClient
import com.simonmarkets.logging.TraceId
import org.mongodb.scala.model.Projections.include
import com.simonmarkets.mongodb.bson.ScalaBsonDocumentsOps.Ops
import io.circe.syntax._

import scala.util.{Failure, Success}
import io.circe.Printer
import io.circe.generic.auto._
import org.mongodb.scala.{Document, MongoCollection}
import org.mongodb.scala.model.Filters.{and, equal, in}
import org.mongodb.scala.model.Sorts.ascending
import java.io.{File, PrintWriter}
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}

object MigrateAccounts extends App {

  private final val HARD_CODED_VARIABLES = ScriptConfig(
    networksDoneFolder = "/your/folder/here", // eg: /Users/bsmith/Documents
    env = DEV, //DEV, QA, or PROD
    tokenSSO = "Simon SSO token string", // get it from Postman, or https://www.{env}.simonmarkets.com/simon/api/config/ui/authtoken
    mongoDbEmail = "yourmongoemail@here.com", // code will take care of encoding this for you
    mongoDbPassword = "yourmongopasswordhere", // this will also get encoded for you
    networks = List.empty[String], // (optional), hardcoded list of networks to migrate
    batchSize = 1000, //(optional) size of accounts page
    checkMongo = false, //(optional) checks pipg.accounts to see if something's missing from pipg.ordersAccounts
    resetCache = false //(optional) clears out all the networksDone JSON which batches were already migrated on which networks
  )

  implicit val as: ActorSystem = ActorSystem.create
  implicit val ec: ExecutionContext = ExecutionContext.global


  private val migrater = MigrateAccountsHelpers.initSetup(HARD_CODED_VARIABLES)
  migrater.runMigration

  System.exit(0)
}

class MigrateAccounts(
  val http: FutureHttpClient,
  val auth: List[RawHeader],
  var doNotInclude: mutable.Map[String, ArrayBuffer[Int]],
  val accountsCollection: MongoCollection[Document],
  val orderAccountsCollection: MongoCollection[Document],
  val networks: List[String],
  val filePrefix: String,
  val env: String,
  val pageSize: Int,
  val checkMongoAfter: Boolean
)(implicit ec: ExecutionContext, as: ActorSystem) {
  val path = s"https://www.${if (env != "prod") env + "." else ""}simonmarkets.com/simon/api"

  def runMigration = {
    println(s"[MigrateAccounts] Starting migration in ${networks.size} networks...")
    networks.foreach(runMigrationHelper(_, 0))
    if (checkMongoAfter) {
      println(s"[MigrateAccounts] Now checking Mongo for missing documents...")
      networks.foreach(checkNetworkInMongo(_, 0))
    }
  }

  private def runMigrationHelper(network: String, batch: Int): Unit = {
    if (!doNotInclude.get(network).exists(_.contains(-1))) {
      val batchDone = (batch + 1) * pageSize
      if (!doNotInclude.get(network).exists(_.contains(batch))) {
        val res = forceRequest(network, batch)
        res match {
          case 0 =>
            cacheDoneBatch(batch, network)
            cacheDoneBatch(-1, network)
          case `batchDone` =>
            cacheDoneBatch(batch, network)
          case _ =>
            println(s"[MigrateAccounts] MIGRATION ERROR, RETRYING: $network-$batch: $res")
            runMigrationHelper(network, batch)
        }
      }
      runMigrationHelper(network, batch + 1)
    }
  }

  private def checkNetworkInMongo(network: String, batchNum: Int = 0): Unit = {
    Await.result(
      for {
        accountsDocs <- accountsCollection
          .find(and(equal("networkId", network), equal("accountType", "Orders")))
          .projection(include("id", "faNumbers"))
          .skip(batchNum * pageSize)
          .limit(pageSize)
          .sort(ascending("_id"))
          .toFuture
        done = accountsDocs.isEmpty
        accounts = accountsDocs.map(doc => doc.getString("id") -> doc.getStringSet("faNumbers"))
        orderAccountsDocs <- if (accounts.isEmpty) Future.successful(Seq.empty[Document]) else
          orderAccountsCollection
            .find(and(equal("networkId", network), in("id", accounts.map(_._1): _*)))
            .projection(include("id", "faNumbers"))
            .toFuture
        orderAccounts = orderAccountsDocs.map(doc => doc.getString("id") -> doc.getStringSet("faNumbers"))
        missingDocs = if (accounts.isEmpty) Seq.empty else accounts.filter(!orderAccounts.contains(_))
        _ = if (missingDocs.nonEmpty) {
          println(s"[MigrateAccounts] $network-$batchNum is missing ${missingDocs.size} ids. Retrying migration...")
          forceRequest(network, batchNum)
          val newDocs = Await.result(orderAccountsCollection
            .find(and(equal("networkId", network), in("id", missingDocs.map(_._1): _*)))
            .projection(include("id", "faNumbers"))
            .toFuture
            .map(_.map(doc => doc.getString("id") -> doc.getStringSet("faNumbers"))), Duration.Inf)
          if (missingDocs.count(!newDocs.contains(_)) != 0) {
            println(s"[MigrateAccounts] $network-$batchNum retry migration failed, ${missingDocs.count(!newDocs.contains(_))} docs missing...")
          } else
            println(s"[MigrateAccounts] Finished migration in $network-$batchNum for ${missingDocs.size} ids.")
        }
        _ = if (!done) checkNetworkInMongo(network, batchNum + 1)

      } yield missingDocs,
      Duration.Inf)

  }

  private def forceRequest(network: String, batch: Int): Int = {
    sendRequest(network, batch) match {
      case res: Int => res
      case x: Any =>
        println(s"[forceRequest] MIGRATION ERROR: RETRYING $network-$batch: $x")
        forceRequest(network, batch)
    }
  }

  private def sendRequest(network: String, batch: Int) = {
    implicit val traceId: TraceId = TraceId(s"$network-$batch")
    val request = MigrateNetworkAccountsRequest(Set(network), Some(batch * pageSize), Some((batch + 1) * pageSize))
    val uri = Uri(s"$path/v2/orders/accounts/migrate")
    val start = System.nanoTime
    val res = Await.result(http.post[MigrateNetworkAccountsRequest, Int](uri = uri, data = request).transformWith {
        case Failure(failure) => Future.successful(failure.getMessage)
        case Success(obj) => Future.successful(obj)
      }, Duration.Inf)
    val elapsedSecs = (System.nanoTime - start) / 1000000000.0
    println(f"[MigrateAccounts] $traceId res=$res elapsed=$elapsedSecs%.3fs")
    res
  }

  private def cacheDoneBatch(batch: Int, network: String): Unit = {
    if (!doNotInclude.get(network).exists(_.contains(batch))) {
      if (!doNotInclude.contains(network)) doNotInclude += network -> new ArrayBuffer[Int]()
      if (batch == -1) doNotInclude(network).prepend(batch) else doNotInclude(network) += batch
      val customPrinter = Printer(
        dropNullValues = false,
        indent = " ",
        lbraceRight = "\n",
        rbraceLeft = "\n",
        lbracketRight = "",
        rbracketLeft = "",
        lrbracketsEmpty = "\n",
        arrayCommaRight = " ",
        objectCommaRight = "\n",
        colonLeft = "",
        colonRight = " ",
        sortKeys = false
      )
      val updatedJsonString = doNotInclude.asJson.printWith(customPrinter)
      val printWriter = new PrintWriter(new File(filePrefix))
      printWriter.write(updatedJsonString)
      printWriter.close()
    }
  }
}




