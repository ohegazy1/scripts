package com.scripts.accounts

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.headers.RawHeader
import com.simonmarkets.http.authentication.{ApiKeyAuthCredentials}
import com.simonmarkets.http.{FutureHttpClient, HttpClientConfig}
import io.circe.Decoder
import org.mongodb.scala.connection.{SocketSettings, SslSettings}
import org.mongodb.scala.model.Projections.include
import org.mongodb.scala.{ConnectionString, MongoClient, MongoClientSettings}
import io.circe.parser._

import java.io.{File, FileOutputStream, PrintWriter}
import java.net.URLEncoder
import java.util.concurrent.TimeUnit
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

object MigrateAccountsHelpers {

  val MONGO_DEV_STRING = "cluster0.pvlpe.mongodb.net/pipg?retryWrites=true&w=majority&authMechanism=PLAIN&authSource=%24external"
  val MONGO_QA_STRING = "cluster-qa1.1zcf4.mongodb.net/pipg?retryWrites=true&w=majority&authMechanism=PLAIN&authSource=%24external"
  val MONGO_PROD_STRING = "cluster-prod1.76wbv.mongodb.net/pipg?retryWrites=true&w=majority&authMechanism=PLAIN&authSource=%24external"

  case class MigrateNetworkAccountsRequest(
    networkIds: Set[String] = Set.empty,
    pageStart: Option[Int] = None,
    pageEnd: Option[Int] = None
  )

  sealed trait Environment extends Product with Serializable

  object Environment {
    case object DEV extends Environment
    case object QA extends Environment
    case object PROD extends Environment
  }

  case class ScriptConfig(
    networksDoneFolder: String,
    env: Environment,
    tokenSSO: String,
    mongoDbEmail: String,
    mongoDbPassword: String,
    networks: List[String] = List.empty,
    batchSize: Int = 1000,
    checkMongo: Boolean = false,
    resetCache: Boolean = false
  )

  /**
   * A hideous way to hide initialization plumbing from the main class.
   * Doesn't matter as long as no one really changes this script!
   */

  def initSetup(HARD_CODED_VARIABLES: ScriptConfig)(implicit as: ActorSystem, ec: ExecutionContext) = {

    val filePath = s"${HARD_CODED_VARIABLES.networksDoneFolder.stripSuffix("/")}/networksDone.json"
    val encodedEmail = URLEncoder.encode(HARD_CODED_VARIABLES.mongoDbEmail, "UTF-8")
    val encodedPassword = URLEncoder.encode(HARD_CODED_VARIABLES.mongoDbPassword, "UTF-8")
    val env = HARD_CODED_VARIABLES.env.productPrefix.toLowerCase
    val checkMongoAfter = HARD_CODED_VARIABLES.checkMongo
    val token = HARD_CODED_VARIABLES.tokenSSO
    val batchQuantity = HARD_CODED_VARIABLES.batchSize
    var ourNetworks = HARD_CODED_VARIABLES.networks

    val mongoDbString = Map(
      "dev" -> s"mongodb+srv://$encodedEmail:$encodedPassword@$MONGO_DEV_STRING",
      "qa" -> s"mongodb+srv://$encodedEmail:$encodedPassword@$MONGO_QA_STRING",
      "prod" -> s"mongodb+srv://$encodedEmail:$encodedPassword@$MONGO_PROD_STRING"
    )(env)


    val http: FutureHttpClient = new FutureHttpClient(Http(), HttpClientConfig(proxy = None, auth = ApiKeyAuthCredentials(token)))

    val auth = List(RawHeader("Authorization", s"Bearer $token"))
    var doNotInclude: mutable.Map[String, ArrayBuffer[Int]] = mutable.Map.empty
    val file = new File(filePath)
    if (!file.exists()) {
      file.createNewFile()
      println(s"[MigrateAccountsHelpers] Created new file: $filePath")
    }

    if (HARD_CODED_VARIABLES.resetCache) {
      println(s"[MigrateAccountsHelper] deleting cache of completed batch migrations")
      val pw = new PrintWriter(new FileOutputStream(
        new File(filePath),
        false), true)
      pw.close()
    }

    val source = scala.io.Source.fromFile(filePath)
    val jsonString = try source.mkString.trim finally source.close()

    implicit val decodeArrayBuffer: Decoder[ArrayBuffer[Int]] = Decoder.decodeSeq[Int].map(seq => ArrayBuffer(seq.sorted: _*))

    implicit val decodeMutableMap: Decoder[mutable.Map[String, ArrayBuffer[Int]]] =
      Decoder.decodeMap[String, ArrayBuffer[Int]].map(map => mutable.Map(map.toSeq: _*))

    doNotInclude = if (jsonString.isEmpty) mutable.Map.empty[String, ArrayBuffer[Int]] else decode[mutable.Map[String, ArrayBuffer[Int]]](jsonString) match {
      case Right(decodedMap) =>
        decodedMap
      case Left(error) =>
        println(s"[MigrateAccountsHelper] Failed to parse JSON: $error")
        mutable.Map.empty[String, ArrayBuffer[Int]]
    }

    val settings = MongoClientSettings.builder()
      .applyConnectionString(new ConnectionString(mongoDbString))
      .applyToSocketSettings(builder => builder.applySettings(SocketSettings.builder()
        .connectTimeout(30, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS)
        .build))
      .applyToSslSettings(builder => builder.applySettings(SslSettings.builder()
        .enabled(true)
        .build()))
      .build()

    lazy val mongoDb = MongoClient(settings).getDatabase("pipg")
    lazy val orderAccountsCollection = mongoDb.getCollection("ordersAccounts")
    lazy val accountsCollection = mongoDb.getCollection("accounts")

    if (ourNetworks.isEmpty) {
      println(s"[MigrateAccountsHelper] getting the ids of all networks in env $env")
      val networksCollection = mongoDb.getCollection("networks_new")
      ourNetworks = Await.result(for {
        networkIdDocs <- networksCollection.find().projection(include("id")).toFuture
        networkIds = networkIdDocs.map(_.getString("id")).toList
      } yield networkIds
        , Duration.Inf
      )
    }
     new MigrateAccounts(http, auth, doNotInclude, accountsCollection, orderAccountsCollection, ourNetworks,
      filePath, env, batchQuantity, checkMongoAfter)
  }



}
